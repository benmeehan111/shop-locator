var mymap = L.map('mapid').setView([51.505, -0.09], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYmVubWVlaGFuIiwiYSI6ImNrbzlseXVkbDB1OGUycXFrcW9nNWo0MWoifQ.W-dWZxXJuEf5XTl0uEna6Q', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap);

let marker=undefined;
function handleSubmit(e) {
	e.preventDefault();
  const shopname={
    name:e.target[0].value
  }
	$.post("location.php", { name: shopname.name },
	   function(data) {
	let loc=JSON.parse(data);
      mymap.setView(new L.LatLng(loc.lat,loc.long), 15);
      if(marker!=undefined){
        mymap.removeLayer(marker);
      }
      marker=L.marker(new L.LatLng(loc.lat,loc.long)).addTo(mymap);
      marker.bindPopup(`<b>${shopname.name}</b>`).openPopup();
}
)
		 
}


